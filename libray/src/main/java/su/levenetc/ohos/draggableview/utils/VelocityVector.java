package su.levenetc.ohos.draggableview.utils;

import ohos.agp.components.VelocityDetector;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by Eugene Levenetc.
 */
public class VelocityVector {
    private static final int DENSITY_XXXHIGH = 640;

    private final AverageValueFilter xVelSmooth = new AverageValueFilter(10, false);
    private final AverageValueFilter yVelSmooth = new AverageValueFilter(10, false);
    private final VelocityDetector velocityTracker;
    private final int densityDpi;
    private float xVelocity;
    private float yVelocity;
    private float maxVel = Float.MAX_VALUE;

    public VelocityVector(VelocityDetector velocityTracker, int densityDpi) {
        this.velocityTracker = velocityTracker;
        this.densityDpi = densityDpi;
    }

    public void setMaxVel(float maxVel) {
        this.maxVel = maxVel;
    }

    public void addMovement(TouchEvent event) {
        velocityTracker.addEvent(event);
        velocityTracker.calculateCurrentVelocity(DENSITY_XXXHIGH / densityDpi, maxVel, maxVel);
        xVelocity = xVelSmooth.handle(velocityTracker.getHorizontalVelocity());
        yVelocity = yVelSmooth.handle(velocityTracker.getVerticalVelocity());
    }

    public float getXVelocity() {
        return xVelocity;
    }

    public float getYVelocity() {
        return yVelocity;
    }
}
